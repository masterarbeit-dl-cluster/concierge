module gitlab.com/masterarbeit-dl-cluster/concierge/cmd/common

go 1.14

require (
	github.com/imdario/mergo v0.3.11 // indirect
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc // indirect
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	k8s.io/client-go v0.0.0-20200813012017-e7a1d9ada0d5
	k8s.io/utils v0.0.0-20200815024018-e34d1aa459f9 // indirect
)


